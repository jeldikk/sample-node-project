const express = require("express");

const PORT = 3000;
const app = express();

app.get("/", (req, res) => {
  res.status(200).json({
    status: "success",
    message: "The system looks fine",
  });
});

app.get("/health", (req, res) => {
  res.status(200).json({
    status: "success",
    systemDate: new Date().toISOString(),
  });
});

app.listen(PORT, () => {
  console.log("app is listening on port ", PORT);
});
